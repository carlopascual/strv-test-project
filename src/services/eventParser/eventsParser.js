import _ from 'lodash';
import eventParser from './eventParser';

export const eventsParser = (events) => {
    events.map(event => {
        eventParser(event);
        return event;
    });

    return events;
};


export default eventsParser;
