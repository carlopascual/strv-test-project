import _ from 'lodash';
import { store } from 'App';
import { OWNED, ATTENDING, VIEW } from './constants';

const eventParser = (event) => {
    const currentID = store.getState().login.response._id;    
    const eventOwnerID = event.owner.id;
    const attendeeIDs = _.map(event.attendees,'id');

    if(eventOwnerID === currentID) event.state = OWNED;
    else if(attendeeIDs.indexOf(currentID) > -1) event.state = ATTENDING;
    else event.state = VIEW;

    return event;
};

export default eventParser;