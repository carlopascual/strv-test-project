import moment from 'moment';

const dateParser = (isoDate) => {
    let date = moment(isoDate);
    date = date.format('MMMM DD YYYY - h:mm A')
    return date;
}

export default dateParser;