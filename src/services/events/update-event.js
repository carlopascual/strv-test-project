import { API_KEY } from 'services/constants';
import eventParser from 'services/eventParser/eventParser';
import { browserHistory } from 'react-router';
import { logout } from 'components/login/forms/sign-in/redux/actions';
import { setAsView, setCurrentEvent } from 'services/events/redux/actions';

const updateEvent = (dispatch, auth, id, body) => {
    var request = new XMLHttpRequest();

    request.open('PATCH', `https://testproject-api-v2.strv.com/events/${id}`);

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);
    request.setRequestHeader('Authorization', auth);

    request.onreadystatechange = function () {
    if (this.readyState === 4) {
        // console.log('Status:', this.status);
        // console.log('Headers:', this.getAllResponseHeaders());
        // console.log('Body:', this.responseText);

        if(this.status === 200) {
            if(this.responseText.length !== 0) {
                dispatch(setCurrentEvent(eventParser(JSON.parse(this.responseText))));
                dispatch(setAsView());
            }
        }
        else if(this.status === 403) {
            // forbidden: log the user out
            dispatch(logout());
        }
        else {
            browserHistory.push('/error');
        }

    }
    };

    request.send(JSON.stringify(body));
}


export default updateEvent;