import { API_KEY } from 'services/constants';
import { browserHistory } from 'react-router';

import attendEvent from './attend-event';

import { logout } from 'components/login/forms/sign-in/redux/actions';

const createEvent = (dispatch, auth, title, description, startsAt, capacity) => {
    var request = new XMLHttpRequest();
    
    request.open('POST', 'https://testproject-api-v2.strv.com/events');

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);
    request.setRequestHeader('Authorization', auth);

    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            // console.log('Status:', this.status);
            // console.log('Headers:', this.getAllResponseHeaders());
            // console.log('Body:', this.responseText);

            if(this.status === 201) {
                if(this.responseText.length !== 0) {
                    //autmoatically join your own event
                    attendEvent(dispatch, JSON.parse(this.responseText)._id, auth);
                    //success, redirect to event page
                    browserHistory.push(`/event/${JSON.parse(this.responseText)._id}`);
                }

            }
            else if(this.status === 403) {
                //forbidden: log the user out
                dispatch(logout());
            }
            else {
                browserHistory.push('/error');
            }
        }
    };

    var body = {
    'title': title,
    'description': description,
    'startsAt': startsAt,
    'capacity': capacity,
    };

    request.send(JSON.stringify(body));
};

export default createEvent;
