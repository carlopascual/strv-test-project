import { API_KEY } from 'services/constants';
import eventsParser from 'services/eventParser/eventsParser';
import { browserHistory } from 'react-router';
import { setEvents } from './redux/actions';

const retrieveEvents = (dispatch) => {
    var request = new XMLHttpRequest();

    request.open('GET', 'https://testproject-api-v2.strv.com/events');

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('Data-Type', 'text');
    request.setRequestHeader('APIKey', API_KEY);

    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            // console.log('Status:', this.status);
            // console.log('Headers:', this.getAllResponseHeaders());
            // console.log('Body:', this.responseText);

            if(this.status === 200) {
                if(this.responseText.length !== 0) {
                    dispatch(setEvents(eventsParser(JSON.parse(this.responseText))));
                }
            }
            else {
                browserHistory.push('/error');
            }
        }
    };

    request.send();    
}

export default retrieveEvents;