import { API_KEY } from 'services/constants';
import eventParser from 'services/eventParser/eventParser';

import { joinEvent } from './redux/actions';
import { logout } from 'components/login/forms/sign-in/redux/actions';

import { browserHistory } from 'react-router';

const attendEvent = (dispatch, eventID, auth) => {
    var request = new XMLHttpRequest();

    request.open('POST', `https://testproject-api-v2.strv.com/events/${eventID}/attendees/me`);

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);
    request.setRequestHeader('Authorization', auth);

    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            // console.log('Status:', this.status);
            // console.log('Headers:', this.getAllResponseHeaders());
            // console.log('Body:', this.responseText);

            if(this.status === 200) {
                if(this.responseText.length !== 0) {
                    dispatch(joinEvent(eventParser(JSON.parse(this.responseText))));
                }
            }
            else if(this.status === 403) {
                // forbidden: log the user out
                dispatch(logout());

            }
            else if(this.status === 400) {
                alert('Sorry! This event is full!');
            }
            else {
                browserHistory.push('/error');
            }
        }
    };

    

    request.send();
};

export default attendEvent;
