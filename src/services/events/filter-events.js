import moment from 'moment';
import _ from 'lodash';

export const getFutureEvents = (events) => {
    return _.compact(_.map(events, (event) => { if(moment(event.startsAt).diff(moment()) > 0) return event }));
};

export const getPastEvents = (events) => {
    return _.compact(_.map(events, (event) => { if(moment(event.startsAt).diff(moment()) < 0) return event }));
};

export const getOwnerEvents = (events, ownerID) => {
    return _.compact(_.map(events, (event) => { if(event.owner._id === ownerID) return event }));
};

