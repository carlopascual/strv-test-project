export const SET_EVENTS = 'SET_EVENTS';
export const SET_CURRENT_EVENT = 'SET_CURRENT_EVENT';
export const JOIN_EVENT = 'JOIN_EVENT';
export const LEAVE_EVENT = 'LEAVE_EVENT';
export const VIEW_EVENT = 'VIEW_EVENT';
export const EDIT_EVENT = 'EDIT_EVENT';

// modes

export const VIEW = 'VIEW';
export const EDIT = 'EDIT';
