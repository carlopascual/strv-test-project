import { SET_EVENTS, SET_CURRENT_EVENT, JOIN_EVENT, LEAVE_EVENT, VIEW_EVENT, EDIT_EVENT } from './constants';

export const setEvents = (events) => ({ type: SET_EVENTS, events });
export const setCurrentEvent = (event) => ({ type: SET_CURRENT_EVENT, event });
export const joinEvent = (event) => ({ type: JOIN_EVENT, event });
export const leaveEvent = (event) => ({ type: LEAVE_EVENT, event });
export const setAsView = (event) => ({ type: VIEW_EVENT, event });
export const setAsEdit = (event) => ({ type: EDIT_EVENT, event });