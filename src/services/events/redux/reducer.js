import _ from 'lodash';
import { SET_EVENTS, SET_CURRENT_EVENT, JOIN_EVENT, LEAVE_EVENT, VIEW_EVENT, EDIT_EVENT, VIEW, EDIT } from './constants';
import { LOGOUT } from 'components/login/forms/sign-in/redux/constants';

const initialState = { events: null, currentEvent: null, view: VIEW };

// http://redux.js.org/docs/recipes/reducers/ImmutableUpdatePatterns.html
const updateEventInEvents = (events, newEvent) => (
    events.map( (event) => {
        if(event.id !== newEvent.id) {
            // This isn't the item we care about - keep it as-is
            return event;
        }

        // Otherwise, this is the one we want - return an updated value
        return {
            ...event,
            ...newEvent,
        };    
    })
);

const Reducer = (state = initialState , action) => {
    switch(action.type) {
        case SET_EVENTS :
            return {
                ...state,
                events: action.events,
                view: VIEW,
            }
        case SET_CURRENT_EVENT :
            return {
                ...state,
                currentEvent: action.event,
            }
        case JOIN_EVENT :
            return {
                ...state,
                events: (state.events !== null && state.events !== false)  && updateEventInEvents(state.events, action.event),
                currentEvent: action.event,
            }
        case LEAVE_EVENT :
            return {
                ...state,
                events: (state.events !== null && state.events !== false) && updateEventInEvents(state.events, action.event),                
                currentEvent: action.event,
            }
        case VIEW_EVENT :
            return {
                ...state,
                view: VIEW,
            }
        case EDIT_EVENT :
            return {
                ...state,
                view: EDIT,
            }
        case LOGOUT : 
            return {
                ...state,
                events: null,
                currentEvent: null,
            }
        default:
            return state
    }
}

export default Reducer;