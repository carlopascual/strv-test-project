import { API_KEY } from 'services/constants';
import eventParser from 'services/eventParser/eventParser';
import { browserHistory } from 'react-router';
import { setCurrentEvent } from 'services/events/redux/actions';

const retrieveEvent = (dispatch, eventID) => {
    var request = new XMLHttpRequest();

    request.open('GET', `https://testproject-api-v2.strv.com/events/${eventID}`);

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);

    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            // console.log('Status:', this.status);
            // console.log('Headers:', this.getAllResponseHeaders());
            // console.log('Body:', this.responseText);

            if(this.status === 200) {
                if(this.responseText.length !== 0) {
                    dispatch(setCurrentEvent(eventParser(JSON.parse(this.responseText))));
                }
            }
            else {
                browserHistory.push('/error'); 
            }
        }   
    };

    request.send();
};

export default retrieveEvent;