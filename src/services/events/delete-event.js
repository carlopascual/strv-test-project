import { API_KEY } from 'services/constants';
import { browserHistory } from 'react-router';

import { logout } from 'components/login/forms/sign-in/redux/actions';

const deleteEvent = (dispatch, auth, id) => {
    var request = new XMLHttpRequest();

    request.open('DELETE', `https://testproject-api-v2.strv.com/events/${id}`);

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);
    request.setRequestHeader('Authorization', auth);

    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            // console.log('Status:', this.status);
            // console.log('Headers:', this.getAllResponseHeaders());
            // console.log('Body:', this.responseText);

            if(this.status === 204) {
                browserHistory.push(`/dashboard`);
            }
            else if(this.status === 403) {
                dispatch(logout());
            }
            else {
                browserHistory.push('/error');
            }
        }
    };

    request.send();
};
export default deleteEvent;
