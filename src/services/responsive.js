import React from 'react';
import MediaQuery from 'react-responsive';

const Component = ({ desktop, mobile }) => (
  <div>
    <MediaQuery minWidth={576}>
      {desktop}
    </MediaQuery>
    <MediaQuery maxWidth={575}>
      {mobile}
    </MediaQuery>
  </div>
);

export default Component;
