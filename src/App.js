import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import {persistStore, autoRehydrate} from 'redux-persist';
import { createStore } from 'redux';
import reducer from './reducer';
import { Provider } from 'react-redux';

// we import application style to load the global styles
// eslint-disable-next-line
import ApplicationStyle from 'static/global-styles/elements';

import Dashboard from 'components/dashboard/';
import Login from 'components/login/';
import EventPage from 'components/event';
import CreateEvent from 'components/create-event';
import FileNotFound from 'components/error-page';
import Profile from 'components/profile';

export const store = createStore(reducer, undefined, autoRehydrate());

const redirectToDashboard = (nextState, replaceState) => {
    replaceState({ nextPathname: nextState.location.pathname }, '/dashboard');
};

const Page404 = () => (
  <FileNotFound is404 />
);

class AppProvider extends React.Component {
  constructor() {
    super();
    this.state = { rehydrated: false };
  }

  componentWillMount() {
    persistStore(store, {}, () => {
      this.setState({rehydrated: true });
    });
  }

  render() {
    if(!this.state.rehydrated) {
      return <div> Loading... </div>;
    }

    return(
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route path='/' component={Dashboard} onEnter={redirectToDashboard}/>    
          <Route path='/login' component={Login} />
          <Route path='/dashboard' component={Dashboard} />
          <Route path='/event/create' component={CreateEvent} />
          <Route path='/event/:id' component={EventPage} />
          <Route path='/profile' component={Profile} />
          <Route path='/error' component={FileNotFound} />
          <Route path='*' component={Page404} />
        </Router>
      </Provider>
    )
  }
}

export default AppProvider;
