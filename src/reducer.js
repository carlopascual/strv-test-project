import { combineReducers } from 'redux';

//import other reducers here
import login from 'components/login/forms/sign-in/redux/reducer';
import view from 'components/dashboard/views/redux/reducer';
import events from 'services/events/redux/reducer';

const Reducer = combineReducers({    
    login,
    events,
    view,
});

export default Reducer;