import React from 'react';
import styled from 'styled-components';

import STORMTROOPER_SVG from 'static/img/layouts/stormtrooper.svg';
import LEFT_PANEL_IMG from 'static/img/login/left-panel.png';
import Logo from 'components/ui-elements/logo';

const QUOTE = '"Great, kid. Don\'t get cocky."';
const QUOTE_AUTHOR = 'Han Solo';

const Outline = styled.div`
    height: 100vh;
    width: 100%;
    display: inline-flex;
`;

const Partition = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;

    &.stormtrooper {
        background: url(${STORMTROOPER_SVG}) no-repeat;
        background-size: 50%;
        background-position: -31% 50%;
    }
`;

const LeftPanel = styled.div`
    width: 40%;
    display: flex;
    flex-direction: column;
    background: #323C46 url(${LEFT_PANEL_IMG}) center;
    background-size: cover;
    position: relative;
`;

const Quote = styled.div`
    font-size: 36px;
    text-align: center;
    width: 60%;
    position: absolute;
    bottom: 0;
    color: #ffffff;
    margin: 0 auto;
    line-height: 1.2;
`;

const GreenLine = styled.div`
    width: 12px;
    height: 2px;
    background: #1BE38B;
`;

const Author = styled.div`
    font-size: 18px;
    color: #949EA8;
`;

const Component = ({children, hasStormTrooper, ...props}) => {
    // prevent scrollbar from appearing when on this page
    document.body.style.overflow = 'hidden';
    document.body.style.backgroundColor = '#FFFFFF';

    return(
        <Outline>
            <LeftPanel>
                <Logo style={{ margin: '40px 0 0 60px' }}/>
                <Quote style={{ left: '0', right: '0', marginBottom: '70px' }} >
                    {QUOTE}
                    <GreenLine style={{ margin: '50px auto 30px'}}/>
                    <Author style={{ margin: '0 auto' }}>{QUOTE_AUTHOR}</Author>
                </Quote> 
            </LeftPanel>
            <Partition className={hasStormTrooper && 'stormtrooper'} style={{ width: '100%' }}>
                {children}
            </Partition>
        </Outline>
    );
};

export default Component;
