import React from 'react';
import styled from 'styled-components';
import Headroom from 'react-headroom';

import Logo from 'components/ui-elements/logo';

const BODY_BACKGROUND_COLOR = '#F9F9FB';

const Header = styled.div`
    width: 100%;
    height: 90px;
    display: flex;
    align-items: center;
    background: rgb(249, 249, 251);
`;

const CloseWrapper = styled.div`
    font-size: 16px;
    position: absolute;
    display: flex;
    align-items: center;
    cursor: pointer;
`;

const XWrapper = styled.div`
    font-size: 19px;
`;

const Link = styled.a`
    text-decoration: none;
    font-color: inherit;
    color: #313C46;
`;

const Component = ({subHeader, children, hasCloseButton, hasX, ...props}) => {
    document.body.style.backgroundColor = BODY_BACKGROUND_COLOR;
    document.body.style.overflow = 'scroll';

    return(
        <div {...props}>
            <Headroom>
                <Header>
                    <Logo black style={{ marginLeft: '30px', zIndex: '2' }} />
                    <Link href="/dashboard">
                       { hasCloseButton && <CloseWrapper style={{ marginRight: '30px', right: '0', zIndex: '2', top: '30px' }}>
                            <XWrapper style={{ marginRight: '10px' }}>✕</XWrapper>
                            {'Close'}
                        </CloseWrapper>}
                        { hasX && <CloseWrapper style={{ marginRight: '30px', right: '0', zIndex: '2', top: '30px' }}>
                            <XWrapper style={{ marginRight: '10px' }}>✕</XWrapper>
                        </CloseWrapper>}
                    </Link>
                    
                </Header>
                {subHeader}
            </Headroom>
            {children}
        </div>
    );
}
export default Component;