import React from 'react';
import styled from 'styled-components';
import Headroom from 'react-headroom';
import { browserHistory } from 'react-router';

import Logo from 'components/ui-elements/logo';
import TRIANGLE_DROPODOWN from 'static/img/layouts/triangle-dropdown.svg';
import Circle from 'components/ui-elements/circle';

import Dropdown from 'components/ui-elements/dropdown';
import { DropdownItem } from 'components/ui-elements/dropdown/elements';

const BODY_BACKGROUND_COLOR = '#F9F9FB';
const CIRCLE_COLOR = '#D9DCE1';
const CIRCLE_FONT_COLOR = '#949EA8';
const DROPDOWN_FONT_COLOR = '#949EA8';

const Header = styled.div`
    width: 100%;
    height: 90px;
    display: flex;
    align-items: center;
    background: rgb(249, 249, 251);
    justify-content: space-between;
`;

const ProfileWrapper = styled.div`
    font-size: 14px;
    color: ${DROPDOWN_FONT_COLOR};
    text-align: center;
    cursor: pointer;
    position: relative;
`;

const RightWrapper = styled.div`
    display: flex;
    align-items: center;
    float: right;
`;

class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isDropdownOpen: false }

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({isDropdownOpen: false});
        }
    }
    
    render() {
        document.body.style.backgroundColor = BODY_BACKGROUND_COLOR;
        document.body.style.overflow = 'scroll';

        const {subHeader, children, logout, firstName, lastName, isMobile, ...props} = this.props;

        return(
            <div {...props}>
                <Headroom>
                    <Header>
                        <Logo black style={{ marginLeft: '30px', zIndex: '2' }} />
                        <RightWrapper style={{ marginRight: '30px', right: '0', zIndex: '2' }}>
                        <Circle onClick={() => {browserHistory.push('/profile');}} backgroundColor={CIRCLE_COLOR} fontColor={CIRCLE_FONT_COLOR} style={{ marginRight: '10px', cursor: 'pointer' }}>{firstName.charAt(0)}{lastName.charAt(0)}</Circle> 
                        <ProfileWrapper onClick={() => { this.setState({ isDropdownOpen: true }); }}>
                            {
                                this.state.isDropdownOpen && 
                                <div style={{ position: 'absolute', left: isMobile ? '-75px' : '-19px', bottom: '-93px' }} ref={this.setWrapperRef}>
                                    <Dropdown triangleLeft={'92px'}>
                                        <DropdownItem onClick={() => {this.setState({ isDropdownOpen: false }); browserHistory.push('/profile');}}>Profile</DropdownItem>
                                        <DropdownItem onClick={logout}>Sign out</DropdownItem>
                                    </Dropdown>
                                </div>
                                    
                            }
                            {!isMobile && `${firstName} ${lastName}`}
                            <img style={{ marginLeft: '2px' }} src={TRIANGLE_DROPODOWN} alt="dropdown" />
                        </ProfileWrapper>
                        </RightWrapper>
                    </Header>
                    {subHeader}
                </Headroom>
                {children}
            </div>
        );
    }
};

//-25px

export default Component;