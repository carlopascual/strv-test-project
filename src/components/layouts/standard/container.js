import { connect } from 'react-redux';
import Component from './component';
import { browserHistory } from 'react-router';

import { logout } from 'components/login/forms/sign-in/redux/actions';

const mapStateToProps = (state) => {
    if(state.login.response === null || state.login.response === false) {
        //redirect to login if not logged in
        browserHistory.push('/login');
        return({
            firstName: '',
            lastName: '',
        });
    }

    return({
        firstName: state.login.response.firstName,
        lastName: state.login.response.lastName,
    });
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    logout: () => { dispatch(logout())}
})

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;