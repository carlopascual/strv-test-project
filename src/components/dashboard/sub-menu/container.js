import Component from './component';
import { connect } from 'react-redux';

import { setViewAsGrid, setViewAsSequence, setFilterAllEvents, setFilterPastEvents, setFilterFutureEvents } from '../views/redux/actions';

const mapStateToProps = (state) => ({
    view: state.view.currentView,
    filter: state.view.filter,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    viewAsGrid: () => { dispatch(setViewAsGrid()); },
    viewAsSequence: () => { dispatch(setViewAsSequence()); },
    filterAllEvents: () => { dispatch(setFilterAllEvents()); },
    filterPastEvents: () => { dispatch(setFilterPastEvents()); },
    filterFutureEvents: () => { dispatch(setFilterFutureEvents()); },
});

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;