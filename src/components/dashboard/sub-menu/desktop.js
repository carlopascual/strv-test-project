import React from 'react';
import styled from 'styled-components';
import MediaQuery from 'react-responsive';

import GridIcon from '../views/grid/grid-icon';
import ListIcon from '../views/list/list-icon';

import { GRID, SEQUENCE, ALL_EVENTS, FUTURE_EVENTS, PAST_EVENTS } from '../views/redux/constants';

const SUB_MENU_ITEM_HOVER_COLOR = '#323C46';
const SUB_MENU_ITEM_TEXT_COLOR = '#A9AFB5';
const SUB_MENU_BACKGROUND_COLOR = '#F9F9FB';

const SubMenuOutline = styled.div`
    height: 30px;
    display: flex;
    align-items: center;
    position: relative;    
    margin: 0 auto;
    background: ${SUB_MENU_BACKGROUND_COLOR};
`;

const SubMenuItem = styled.div`
    display: flex;
    align-items: center;
    color: ${props => props.isSelected ? SUB_MENU_ITEM_HOVER_COLOR : SUB_MENU_ITEM_TEXT_COLOR};
    font-size: 12px;
    
    &:hover {
        color: ${SUB_MENU_ITEM_HOVER_COLOR};
        cursor: pointer;
    }

    &.selected {
        color ${SUB_MENU_ITEM_HOVER_COLOR};
    }
`;

const SubMenuItemWrapper = styled.div`
    display: inline-flex;
    align-items: center;
`;

const SubMenu = ({ isMobile, filterAllEvents, filterPastEvents, filterFutureEvents, viewAsGrid, viewAsSequence, view, filter, menuStyle, ...props }) => (
    <SubMenuOutline style={menuStyle} >
        <SubMenuItemWrapper>
            <SubMenuItem onClick={filterAllEvents} isSelected={filter === ALL_EVENTS} style={{ marginRight:'10px' }}>ALL EVENTS</SubMenuItem>
            <SubMenuItem onClick={filterFutureEvents} isSelected={filter === FUTURE_EVENTS} style={{ marginRight:'10px' }}>FUTURE EVENTS</SubMenuItem>
            <SubMenuItem onClick={filterPastEvents} isSelected={filter === PAST_EVENTS} >PAST EVENTS</SubMenuItem>
            <div style={{ alignItems: 'center', position: 'absolute', right: '0', display: 'flex' }}>
                <GridIcon onClick={viewAsGrid} isSelected={view === GRID} style={{ marginRight: '5px', cursor: 'pointer' }}/>
                <ListIcon onClick={viewAsSequence} isSelected={view === SEQUENCE} style={{ cursor: 'pointer' }} />
            </div>
        </SubMenuItemWrapper>
    </SubMenuOutline>
);

const Component = ({children, ...props}) => (
    <div>
        <MediaQuery menuStyle={{ width: '1240px' }} minWidth={1430}>
            <SubMenu {...props} />
        </MediaQuery>
        <MediaQuery menuStyle={{ width: '820px' }} maxWidth={1430} minWidth={1018}>
            <SubMenu {...props} />            
        </MediaQuery>
        <MediaQuery menuStyle={{ width: '90%' }} maxWidth={1018}>
            <SubMenu {...props} />                            
        </MediaQuery>
    </div>
);

export default Component;
