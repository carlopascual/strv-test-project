import React from 'react';
import styled from 'styled-components';
import MediaQuery from 'react-responsive';

import GridIcon from '../views/grid/grid-icon';
import ListIcon from '../views/list/list-icon';

import TRIANGLE_DROPODOWN from 'static/img/layouts/triangle-dropdown.svg';

import { GRID, SEQUENCE, ALL_EVENTS, FUTURE_EVENTS, PAST_EVENTS } from '../views/redux/constants';

import Dropdown from 'components/ui-elements/dropdown';
import { DropdownItem } from 'components/ui-elements/dropdown/elements';

const SUB_MENU_ITEM_HOVER_COLOR = '#323C46';
const SUB_MENU_ITEM_TEXT_COLOR = '#A9AFB5';
const SUB_MENU_BACKGROUND_COLOR = '#F9F9FB';

const SHOW_TEXT = 'SHOW:';
const SHOW_TEXT_COLOR = '#A9AFB5';

const SubMenuOutline = styled.div`
    height: 30px;
    display: flex;
    align-items: center;
    position: relative;    
    margin: 0 auto;
    background: ${SUB_MENU_BACKGROUND_COLOR};
`;

const SubMenuItem = styled.div`
    display: flex;
    align-items: center;
    color: ${props => props.isSelected ? SUB_MENU_ITEM_HOVER_COLOR : SUB_MENU_ITEM_TEXT_COLOR};
    font-size: 12px;
    
    &:hover {
        color: ${SUB_MENU_ITEM_HOVER_COLOR};
        cursor: pointer;
    }

    &.selected {
        color ${SUB_MENU_ITEM_HOVER_COLOR};
    }
`;

const SubMenuItemWrapper = styled.div`
    display: inline-flex;
    align-items: center;
`;

const Show = styled.div`
    font-size: 12px;
    color: ${SHOW_TEXT_COLOR};
`;

const Outline = styled.div`
    background: ${SUB_MENU_BACKGROUND_COLOR};
`;

class SubMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isDropdownOpen: false };

        this.setWrapperRef = this.setWrapperRef.bind(this);           
        this.handleClickOutside = this.handleClickOutside.bind(this);
    };

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({isDropdownOpen: false});
        }
    }

    render() {
        //eslint-disable-next-line
        const { filterAllEvents, filterPastEvents, filterFutureEvents, viewAsGrid, viewAsSequence, view, filter, menuStyle, ...props } = this.props;

        let currentlySelected = 'ALL EVENTS';

        if(filter === FUTURE_EVENTS) currentlySelected = 'FUTURE EVENTS';
        if(filter === PAST_EVENTS) currentlySelected = 'PAST EVENTS';

        return(
            <Outline>
            <SubMenuOutline style={menuStyle} >
                <SubMenuItemWrapper>
                    <Show style={{ marginRight: '5px' }}>{SHOW_TEXT}</Show>
                    <SubMenuItem onClick={() => { this.setState({isDropdownOpen: true}); } } isSelected={true} style={{ marginRight:'5px' }}>{currentlySelected}</SubMenuItem>
                    { this.state.isDropdownOpen && 
                        <div style={{ position: 'absolute', left: '35px', top: '37px' }}ref={this.setWrapperRef}>
                            <Dropdown triangleLeft={'48px'}>
                                <DropdownItem 
                                    onClick={() => { this.setState({isDropdownOpen: false }); filterAllEvents(); }} 
                                    isSelected={filter === ALL_EVENTS}
                                >{'All Events'}
                                </DropdownItem>
                                <DropdownItem 
                                    onClick={() => { this.setState({isDropdownOpen: false }); filterFutureEvents(); }} 
                                    isSelected={filter === FUTURE_EVENTS}
                                >{'Future Events'}
                                </DropdownItem>
                                <DropdownItem 
                                    onClick={() => { this.setState({isDropdownOpen: false }); filterPastEvents(); }} 
                                    isSelected={filter === PAST_EVENTS}
                                >{'Past Events'}
                                </DropdownItem>
                            </Dropdown>
                        </div>
                    }
                    <img style={{ marginBottom: '3px' }}src={TRIANGLE_DROPODOWN} alt="dropdown" />

                    <div style={{ alignItems: 'center', position: 'absolute', right: '0', display: 'flex' }}>
                        <GridIcon onClick={viewAsGrid} isSelected={view === GRID} style={{ marginRight: '5px', cursor: 'pointer' }}/>
                        <ListIcon onClick={viewAsSequence} isSelected={view === SEQUENCE} style={{ cursor: 'pointer' }} />
                    </div>
                </SubMenuItemWrapper>
            </SubMenuOutline>
            </Outline>
            
        );
    };
};

const Component = ({children, ...props}) => (
    <div>
        <MediaQuery menuStyle={{ width: '85%' }} maxWidth={1018}>
            <SubMenu {...props} />                            
        </MediaQuery>
    </div>
);

export default Component;
