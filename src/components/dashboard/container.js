import Component from './component';
import { connect } from 'react-redux';
import { store } from 'App';
 
import retrieveEvents from 'services/events/retrieve-all-events';

import { FUTURE_EVENTS, PAST_EVENTS } from './views/redux/constants';
import { getFutureEvents, getPastEvents } from 'services/events/filter-events';

const mapStateToProps = (state) => {
    let eventsToReturn = state.events.events;
    
    if(state.view.filter === FUTURE_EVENTS) {
       eventsToReturn =  getFutureEvents(eventsToReturn);
    }
    else if(state.view.filter === PAST_EVENTS) {
       eventsToReturn =  getPastEvents(eventsToReturn);        
    }

    return({
        events: eventsToReturn,
        view: state.view.currentView,
    });
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    retrieveEvents: () => { if(store.getState().login.response !== null) retrieveEvents(dispatch); }
});

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;