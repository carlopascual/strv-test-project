import React from 'react';
import styled from 'styled-components';

import Grid from './views/grid';
import List from './views/list';

import StandardLayout from 'components/layouts/standard';

import AddWidget from 'components/ui-elements/add-widget';
import { GRID } from './views/redux/constants';

import SubMenu from './sub-menu';

const GridOutline = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const ListOutline = styled.div`
    display: block;
`;

class Component extends React.Component {
    // eslint-disable-next-line
    constructor(props) {
        super(props);
    }
    
    componentWillMount() {
        this.props.retrieveEvents();
    }

    render() {
        const { view, isMobile } = this.props;

        return(
            <StandardLayout isMobile={isMobile} subHeader={<SubMenu isMobile={isMobile} />}>
                {
                    view === GRID ? 
                        <GridOutline style={{ marginBottom: '100px' }}>                  
                            <Grid isMobile={isMobile}>{this.props.events}</Grid>
                        </GridOutline> 
                    :
                        <ListOutline style={{ marginBottom: '100px' }}>                  
                            <List isMobile={isMobile}>{this.props.events}</List>
                        </ListOutline> 
                }
                <AddWidget style={{ position: 'fixed', bottom: '30px', right: '30px' }} />                                
            </StandardLayout>
        );
    }
};

// 660

export default Component;
