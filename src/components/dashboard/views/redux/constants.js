export const GRID = 'GRID';
export const SEQUENCE = 'SEQUENCE';

export const VIEW_AS_GRID = 'VIEW_AS_GRID';
export const VIEW_AS_SEQUENCE = 'VIEW_AS_SEQUENCE';

export const ALL_EVENTS = 'ALL_EVENTS';
export const FUTURE_EVENTS = 'FUTURE_EVENTS';
export const PAST_EVENTS = 'PAST_EVENTS';

export const VIEW_ALL_EVENTS = 'VIEW_ALL_EVENTS';
export const VIEW_FUTURE_EVENTS = 'VIEW_FUTURE_EVENTS';
export const VIEW_PAST_EVENTS = 'VIEW_PAST_EVENTS';