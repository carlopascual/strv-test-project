import { VIEW_AS_GRID, VIEW_AS_SEQUENCE, VIEW_ALL_EVENTS, VIEW_PAST_EVENTS, VIEW_FUTURE_EVENTS } from './constants';

export const setViewAsGrid = () => ({ type: VIEW_AS_GRID });
export const setViewAsSequence = () => ({ type: VIEW_AS_SEQUENCE });

export const setFilterAllEvents = () => ({ type: VIEW_ALL_EVENTS });
export const setFilterPastEvents = () => ({ type: VIEW_PAST_EVENTS });
export const setFilterFutureEvents = () => ({ type: VIEW_FUTURE_EVENTS });

