import { VIEW_AS_GRID, VIEW_AS_SEQUENCE, VIEW_ALL_EVENTS, VIEW_PAST_EVENTS, VIEW_FUTURE_EVENTS, GRID, SEQUENCE, ALL_EVENTS, PAST_EVENTS, FUTURE_EVENTS} from './constants';

const initialState = { currentView: SEQUENCE, filter: ALL_EVENTS};

const Reducer = (state = initialState , action) => {
    switch(action.type) {
        case VIEW_AS_GRID :
            return {
                ...state,
                currentView: GRID,
            }
        case VIEW_AS_SEQUENCE :
            return {
                ...state,
                currentView: SEQUENCE,
            }
         case VIEW_ALL_EVENTS :
            return {
                ...state,
                filter: ALL_EVENTS,
            }
         case VIEW_FUTURE_EVENTS :
            return {
                ...state,
                filter: FUTURE_EVENTS,
            }
         case VIEW_PAST_EVENTS :
            return {
                ...state,
                filter: PAST_EVENTS,
            }
        default:
            return state
    }
}

export default Reducer;
