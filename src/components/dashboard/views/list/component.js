import React from 'react';
import styled from 'styled-components';
import MediaQuery from 'react-responsive';

import ListItem from './list-item';

const Outline = styled.div`
    margin: 0 auto;
`;

const Component = ({children, isMobile, ...props}) => (
    <div {...props}>
        <MediaQuery minWidth={1430}>
            <Outline style={{ width: '1240px' }}>
            {
                !(children == null) && children.map(event => (
                        <ListItem key={event._id}>{event}</ListItem> 
                ))
            }
            </Outline>
        </MediaQuery>
        <MediaQuery maxWidth={1430} minWidth={1018}>
            <Outline style={{ width: '820px' }}>
            {
                !(children == null) && children.map(event => (
                        <ListItem key={event._id}>{event}</ListItem> 
                ))
            }
            </Outline>
        </MediaQuery>
        <MediaQuery maxWidth={1018}>
            <Outline style={{ width: '90%' }}>
            {
                !(children == null) && children.map(event => (
                        <ListItem isMobile={isMobile} key={event._id}>{event}</ListItem> 
                ))
            }             
            </Outline>
        </MediaQuery>
    </div>
);

export default Component;
