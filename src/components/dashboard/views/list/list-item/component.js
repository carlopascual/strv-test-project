import React from 'react';
import styled from 'styled-components';
import dateParser from 'services/dateParser';

import Button from 'components/ui-elements/button';

import { OWNED, ATTENDING, VIEW } from 'services/eventParser/constants';

const TITLE_COLOR = '#323C46';
const TIMESTAMP_COLOR = '#CACDD0';
const OWNER_COLOR = '#7D7878';
const DESCRIPTION_COLOR = '#949EA8';
const ATTENDEES_COLOR = '#949EA8';

const Outline = styled.div`
    background: #FFFFFF;
    width: 100%;
    height: 100%;
    box-shadow: 0px 2px 3px #888888;
    display: flex;
    margin-bottom: 15px;
`;

const Title = styled.div`
    font-size: 18px;
    color: ${TITLE_COLOR};
    white-space: nowrap;
    width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const TimeStamp = styled.div`
    font-size: 14px;
    color: ${TIMESTAMP_COLOR};
    width: 200px;
`;

const Owner = styled.div`
    font-size: 14px;
    color: ${OWNER_COLOR};
    width: 100px;
`;

const Description = styled.div`
    font-size: 16px;
    color: ${DESCRIPTION_COLOR};
    white-space: nowrap;
    width: ${props => props.isMobile ? '50vw' : '369px'};
    overflow: hidden;
    text-overflow: ellipsis;    
`;

const Attendees = styled.div`
    font-size: 14px;
    color: ${ATTENDEES_COLOR};
    width: 80px;
`;

const TitleLink = styled.a`
    text-decoration: none;
    color: ${TITLE_COLOR};

    &:hover {
        color: #000000;
    }j
`;

const Component = ({ authorization, onLeave, onEdit, onJoin, isOnEventPage, children, isMobile, ...props}) => (
    <Outline {...props}>
        <div style={{ display: 'flex', alignItems: 'center', padding: '20px', flexWrap: 'wrap' }}>
            <Title style={{ marginRight: '30px'}} >
                <TitleLink href={`/event/${children.id}`}>{children.title}</TitleLink>
            </Title>         
            <Description isMobile={isMobile} style={{ marginRight: '30px'}} >{children.description}</Description>
            <Owner style={{ marginRight: '30px' }}>{children.owner.firstName} {children.owner.lastName}</Owner>
            <TimeStamp style={{ marginRight: '30px' }}>{dateParser(children.startsAt)}</TimeStamp>
            <Attendees style={{ marginRight: '30px' }}>{children.attendees.length} of {children.capacity}</Attendees>
             <Button 
                style={{  
                    display: `${children.state !== VIEW ? 'none' : 'inline' }`,
                    margin: `${isMobile && 'auto'}`,
                }} 
                className="small green"
                onClick={() => {onJoin(children.id, authorization)}}
            > JOIN </Button>
            <Button 
                style={{   
                    display: `${children.state !== ATTENDING ? 'none' : 'inline' }`,
                    margin: `${isMobile && 'auto'}`,
                }} 
                className="small pink"
                onClick={() => {onLeave(children.id, authorization)}}                    
            > LEAVE </Button>
            <Button 
                style={{  
                    background: '#D9DCE1', 
                    display: `${children.state !== OWNED ? 'none' : 'inline' }`,
                    margin: `${isMobile && 'auto'}`,
                }} 
                className="small"
                onClick={() => { onEdit(children.id); }}
            > EDIT </Button>
        </div>
    </Outline>
);

export default Component;
