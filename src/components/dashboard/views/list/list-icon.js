import React from'react';
import styled from 'styled-components';

const FILLED_COLOR = '#323C46';
const UNFILLED_COLOR = '#D9DCE1';

const Svg = styled.svg`
    > g, path {
        fill: ${props => props.isSelected ? FILLED_COLOR : UNFILLED_COLOR};
    }

    &:hover {
        > g, path {
            fill: ${FILLED_COLOR};
        }
    }
`;

const Component = ({ isSelected, ...props }) => (
    <div {...props}>
        <Svg isSelected={isSelected} width="17px" height="13px" viewBox="0 0 17 13" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="01-1-2-Dashboard-List-View" transform="translate(-1303.000000, -174.000000)" >
                    <g id="Switcher" transform="translate(1267.000000, 169.000000)">
                        <g id="list-view" transform="translate(32.000000, 0.000000)">
                            <path d="M3.99999976,17.9999989 L20.9999987,17.9999989 L20.9999987,11.9999993 L3.99999976,11.9999993 L3.99999976,17.9999989 Z M3.99999976,4.9999997 L3.99999976,10.9999993 L20.9999987,10.9999993 L20.9999987,4.9999997 L3.99999976,4.9999997 Z" id="Fill-2"></path>
                        </g>
                    </g>
                </g>
            </g>
        </Svg>
    </div>
);

export default Component;