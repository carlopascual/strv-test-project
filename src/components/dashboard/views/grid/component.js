import React from 'react';
import styled from 'styled-components';
import MediaQuery from 'react-responsive';

import GridItem from './grid-item';

const Outline = styled.div`
    display: grid;
    grid-gap: 20px;
`;

const Component = ({children, isMobile, ...props}) => (
    <div {...props}>
        <MediaQuery minWidth={1430}>
            <Outline style={{ gridTemplateColumns: '400px 400px 400px' }}>
            {
                !(children == null) && children.map(event => (
                        <GridItem key={event._id}>{event}</GridItem> 
                ))
            }
            </Outline>
        </MediaQuery>
        <MediaQuery maxWidth={1430} minWidth={1018}>
            <Outline style={{ gridTemplateColumns: '400px 400px' }}>
            {
                !(children == null) && children.map(event => (
                        <GridItem key={event._id}>{event}</GridItem> 
                ))
            }
            </Outline>
        </MediaQuery>
        <MediaQuery minWidth={417} maxWidth={1018}>
            <Outline style={{ gridTemplateColumns: '400px' }}>
            {
                !(children == null) && children.map(event => (
                        <GridItem key={event._id}>{event}</GridItem> 
                ))
            }             
            </Outline>
        </MediaQuery>
        <MediaQuery maxWidth={416}>
            <Outline style={{ gridTemplateColumns: 'auto' }}>
            {
                !(children == null) && children.map(event => (
                        <GridItem style={{ width: '89vw', margin: '0 auto'}} key={event._id}>{event}</GridItem> 
                ))
            }             
            </Outline>
        </MediaQuery>
    </div>
);

export default Component;
