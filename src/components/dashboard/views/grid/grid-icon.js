import React from'react';
import styled from 'styled-components';

const FILLED_COLOR = '#323C46';
const UNFILLED_COLOR = '#D9DCE1';

const Svg = styled.svg`
    > g, path {
        fill: ${props => props.isSelected ? FILLED_COLOR : UNFILLED_COLOR};
    }

    &:hover {
        > g, path {
            fill: ${FILLED_COLOR};
        }
    }
`;

const Component = ({ isSelected, ...props }) => (
    <div {...props}>
        <Svg isSelected={isSelected} width="17px" height="13px" viewBox="0 0 17 13" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="01-1-2-Dashboard-List-View" transform="translate(-1271.000000, -174.000000)" >
                    <g id="Switcher" transform="translate(1267.000000, 169.000000)">
                        <g id="grid">
                            <path d="M3.99999976,10.9999993 L8.99999946,10.9999993 L8.99999946,4.9999997 L3.99999976,4.9999997 L3.99999976,10.9999993 Z M3.99999976,17.9999989 L8.99999946,17.9999989 L8.99999946,11.9999993 L3.99999976,11.9999993 L3.99999976,17.9999989 Z M9.9999994,17.9999989 L14.9999991,17.9999989 L14.9999991,11.9999993 L9.9999994,11.9999993 L9.9999994,17.9999989 Z M15.999999,17.9999989 L20.9999987,17.9999989 L20.9999987,11.9999993 L15.999999,11.9999993 L15.999999,17.9999989 Z M9.9999994,10.9999993 L14.9999991,10.9999993 L14.9999991,4.9999997 L9.9999994,4.9999997 L9.9999994,10.9999993 Z M15.999999,4.9999997 L15.999999,10.9999993 L20.9999987,10.9999993 L20.9999987,4.9999997 L15.999999,4.9999997 Z" id="Fill-2"></path>
                        </g>
                    </g>
                </g>
            </g>
        </Svg>
    </div>
);

export default Component;