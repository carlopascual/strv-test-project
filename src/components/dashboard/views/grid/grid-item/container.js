import Component from './component';
import { connect } from 'react-redux';
import attendEvent from 'services/events/attend-event';
import leaveEvent from 'services/events/leave-event';
import { setAsEdit } from 'services/events/redux/actions';
import { browserHistory } from 'react-router';

const mapStateToProps = (state) => ({
    authorization: state.login.response.authorization,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    onJoin: (eventID, authorization) => {attendEvent(dispatch, eventID, authorization )},
    onLeave: (eventID, authorization) => {leaveEvent(dispatch, eventID, authorization )},
    onEdit: (eventID) => { browserHistory.push(`/event/${eventID}`); dispatch(setAsEdit());},
});

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;
