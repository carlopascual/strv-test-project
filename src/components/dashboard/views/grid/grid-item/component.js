import React from 'react';
import styled from 'styled-components';
import dateParser from 'services/dateParser';

import USER_ICON from 'static/img/event/user-icon.svg';
import Button from 'components/ui-elements/button';

import { OWNED, ATTENDING, VIEW } from 'services/eventParser/constants';

const TITLE_COLOR = '#323C46';
const TIMESTAMP_COLOR = '#CACDD0';
const OWNER_COLOR = '#7D7878';
const DESCRIPTION_COLOR = '#949EA8';
const ATTENDEES_COLOR = '#949EA8';

const Outline = styled.div`
    background: #FFFFFF;
    width: 100%;
    height: 100%;
    box-shadow: 0px 2px 3px #888888;
`;

const Title = styled.div`
    height: 35px;
    font-size: 22px;
    color: ${TITLE_COLOR};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    &.on-event-page {
        overflow: visible;
        white-space: normal;
        height: 100%;
        font-size: 35px;
    }
`;

const TimeStamp = styled.div`
    font-size: 14px;
    color: ${TIMESTAMP_COLOR};
`;

const Owner = styled.div`
    font-size: 14px;
    color: ${OWNER_COLOR};
`;

const Description = styled.div`
    font-size: 16px;
    color: ${DESCRIPTION_COLOR};
    width: 80%;
    overflow: hidden;
    height: 100px;
    word-wrap: break-word;

    &:hover {
        overflow-y: auto;
    }

    &.on-event-page {
        overflow: visible;
        height: 100%;
    }
`;

const Wrapper = styled.div`
    display: inline-flex;
    align-items: center;
    width: 100%;
    position: relative;
`;

const Attendees = styled.div`
    font-size: 14px;
    color: ${ATTENDEES_COLOR};
`;

const TitleLink = styled.a`
    text-decoration: none;
    color: ${TITLE_COLOR};

    &:hover {
        color: #000000;
    }j
`;

const Component = ({ authorization, onLeave, onEdit, onJoin, isOnEventPage, children, ...props}) => (
    <Outline {...props} >
        <div style={{ padding: '50px'}}>
            <TimeStamp style={{ marginBottom: '20px' }}>{dateParser(children.startsAt)}</TimeStamp>
            <Title className={isOnEventPage && 'on-event-page'}>
                <TitleLink href={`/event/${children.id}`}>{children.title}</TitleLink>
            </Title>
            <Owner style={{ marginBottom: '20px' }}>{children.owner.firstName} {children.owner.lastName}</Owner>
            <Description className={isOnEventPage && 'on-event-page'} style={{ marginBottom: '20px' }}>{children.description}</Description>
            <Wrapper>
                
                <img style={{ marginRight: '6px' }} src={USER_ICON} alt="user icon" />
                <Attendees>{children.attendees.length} of {children.capacity}</Attendees>
                <Button 
                    style={{ 
                        position: 'absolute', 
                        right: '0', 
                        display: `${children.state !== VIEW ? 'none' : 'inline' }` 
                    }} 
                    className="small green"
                    onClick={() => {onJoin(children.id, authorization)}}
                > JOIN </Button>
                <Button 
                    style={{ 
                        position: 'absolute', 
                        right: '0', 
                        display: `${children.state !== ATTENDING ? 'none' : 'inline' }` 
                    }} 
                    className="small pink"
                    onClick={() => {onLeave(children.id, authorization)}}                    
                > LEAVE </Button>
                <Button 
                    style={{ 
                        position: 'absolute', 
                        right: '0',
                        background: '#D9DCE1', 
                        display: `${children.state !== OWNED ? 'none' : 'inline' }` 
                    }} 
                    className="small"
                    onClick={() => {onEdit(children.id); }}
                > EDIT </Button>
            </Wrapper>
        </div>
    </Outline>
);

export default Component;
