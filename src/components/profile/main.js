import React from 'react';
import styled from 'styled-components';

import Header from './header';

import StandardLayout from 'components/layouts/standard';

import Grid from 'components/dashboard/views/grid';
import List from 'components/dashboard/views/list';

import GridIcon from 'components/dashboard/views/grid/grid-icon';
import ListIcon from 'components/dashboard/views/list/list-icon';

import { GRID, SEQUENCE } from 'components/dashboard/views/redux/constants';

import MediaQuery from 'react-responsive';

const MENU_TEXT_COLOR = '#323C46';
const MENU_TEXT = 'My Events';

const Menu = styled.div`
    color: ${MENU_TEXT_COLOR};
    font-size: 28px;
    position: relative;
    display: flex;
    margin: 0 auto;
`;

const GridOutline = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const ListOutline = styled.div`
    display: block;
`;

class Component extends React.Component {
     // eslint-disable-next-line
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.retrieveEvents();
    }

    render() {
        // eslint-disable-next-line
        const { view, viewAsGrid, viewAsSequence, isMobile, ...props} = this.props;
        
        return(
            <StandardLayout isMobile={isMobile}>
                <Header style={{ marginBottom: '20px'}} />
                <ReactiveMenu view={view} viewAsGrid={viewAsGrid} viewAsSequence={viewAsSequence} style={{ marginBottom: '20px' }}/>
                {
                    view === GRID ? 
                        <GridOutline style={{ marginBottom: '70px' }}>                  
                            <Grid isMobile={isMobile}>{this.props.events}</Grid>
                        </GridOutline> 
                    :
                        <ListOutline>                  
                            <List isMobile={isMobile}>{this.props.events}</List>
                        </ListOutline> 
                }
            </StandardLayout>
        );
    }
};

const SubMenu = ({ viewAsGrid, viewAsSequence, view, menuStyle, ...props }) => (
    <Menu style={menuStyle}>
        <div>{MENU_TEXT}</div>
        <div style={{ alignItems: 'center', position: 'absolute', right: '0', display: 'flex' }}>
            <GridIcon onClick={viewAsGrid} isSelected={view === GRID} style={{ marginRight: '5px', cursor: 'pointer' }}/>
            <ListIcon onClick={viewAsSequence} isSelected={view === SEQUENCE} style={{ cursor: 'pointer' }}/>
        </div>
    </Menu>
);

const ReactiveMenu = ({children, style, ...props}) => (
    <div style={style}>
        <MediaQuery menuStyle={{ width: '1240px' }} minWidth={1430}>
            <SubMenu {...props} />
        </MediaQuery>
        <MediaQuery menuStyle={{ width: '820px' }} maxWidth={1430} minWidth={1018}>
            <SubMenu {...props} />            
        </MediaQuery>
        <MediaQuery menuStyle={{ width: '90%' }} maxWidth={1018}>
            <SubMenu {...props} />                            
        </MediaQuery>
    </div>
);

export default Component;