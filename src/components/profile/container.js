import Component from './component';
import { connect } from 'react-redux';

import retrieveEvents from 'services/events/retrieve-all-events';
import { getOwnerEvents } from 'services/events/filter-events';
import { browserHistory } from 'react-router';
import { store } from 'App';

import { setViewAsGrid, setViewAsSequence } from 'components/dashboard/views/redux/actions';

const mapStateToProps = (state) => {
    let eventsToReturn = state.events.events;

    if(state.login.response === null) {
        browserHistory.push('/login');
        return;
    }
    
    eventsToReturn = getOwnerEvents(eventsToReturn, state.login.response._id);

    return({
        events: eventsToReturn,
        view: state.view.currentView,
    });
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    retrieveEvents: () => { if(store.getState().login.response !== null) retrieveEvents(dispatch); },
    viewAsGrid: () => { dispatch(setViewAsGrid()); },
    viewAsSequence: () => { dispatch(setViewAsSequence()); },
});

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;