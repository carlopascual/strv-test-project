import React from 'react';
import styled from 'styled-components';

import MediaQuery from 'react-responsive';

const CIRCLE_BACKGROUND = '#D9DCE1';
const CIRLCE_TEXT = '#A9AEB4';

const NAME_COLOR = '#323C46';
const EMAIL_COLOR = '#949EA8';

const Outline = styled.div`
    background: #FFFFFF;
    width: 100%;
    box-shadow: 0px 2px 3px #888888;
`;

const Circle = styled.div`
    border-radius: 50%;
    width: 120px;
    height: 120px;
    text-align: center;
    font-size: 48px;
    line-height: 130px;
    background-color: ${CIRCLE_BACKGROUND};
    color: ${CIRLCE_TEXT};
`;

const Wrapper = styled.div`
    height: 100px;
    padding: 20px;
    padding-top: 60px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const Name = styled.div`
    color: ${NAME_COLOR};
    font-size: 18px;
`;

const Email = styled.div`
    color: ${EMAIL_COLOR};
    font-size: 14px;
`;

const Component = ({ firstName, lastName, email, headerStyle, ...props }) => (
    <div style={headerStyle}>
        <div style={{ position: 'relative', height: '60px' }}>
            <Circle style={{ margin: '0 auto' }}>{firstName.charAt(0)}{lastName.charAt(0)}</Circle>            
        </div>
        <Outline>
            <Wrapper>
                <Name>{firstName} {lastName}</Name>
                <Email>{email}</Email>
            </Wrapper>
        </Outline>
    </div>
)

const ReactiveHeader = ({children, style, ...props}) => (
    <div style={style}>
        <MediaQuery headerStyle={{ width: '1240px', margin: '0 auto' }} minWidth={1430}>
            <Component {...props} />
        </MediaQuery>
        <MediaQuery headerStyle={{ width: '820px', margin: '0 auto' }} maxWidth={1430} minWidth={1018}>
            <Component {...props} />            
        </MediaQuery>
        <MediaQuery headerStyle={{ width: '90%', margin: '0 auto' }} maxWidth={1018}>
            <Component {...props} />                            
        </MediaQuery>
    </div>
);

export default ReactiveHeader;