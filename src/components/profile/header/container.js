import Component from './component';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

const mapStateToProps = (state) => {

    if(state.login.response === null) {
        browserHistory.push('/login');
        return({
            firstName: '',
            lastName: '',
            email: '',
        });
    }

    return({
        firstName: state.login.response.firstName,
        lastName: state.login.response.lastName,
        email: state.login.response.email,
    })
};

const Composer = connect(
    mapStateToProps,
    null,
)(Component);

export default Composer;