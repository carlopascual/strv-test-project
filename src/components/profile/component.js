import React from 'react';

import Responsive from 'services/responsive';
import Main from './main';

const Component = ({ ...props }) => (
    <Responsive desktop={<Main {...props} />} mobile={<Main isMobile {...props} />}/>
);

export default Component;
