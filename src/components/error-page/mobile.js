import React from 'react';
import styled from 'styled-components';

import MinimalLayout from 'components/layouts/minimal';
import Button from 'components/ui-elements/button';

import { browserHistory } from 'react-router';
import STORMTROOPER_SVG from 'static/img/layouts/stormtrooper.svg';

const TITLE = 'Something went wrong.';
const TITLE_4O4 = '404 Error - Page not found.';
const TITLE_COLOR = '#323C46';

const DESCRIPTION = 'Seems like Darth Vader just hits our website and drops it down. Please press the refresh button and everything should be fine again.';
const DESCRIPTION_COLOR = '#949EA8';

const BUTTON_TEXT = 'REFRESH';
const BUTTON_COLOR = '#323C46';

const Outline = styled.div`
    width: 70%;
    margin: 0 auto;
   
`;

const Title = styled.div`
    font-size: 28px;
    color: ${TITLE_COLOR};
`;

const Description = styled.div`
    font-size: 18px;
    color: ${DESCRIPTION_COLOR};
`;

const Wrapper = styled.div`
    background: url(${STORMTROOPER_SVG}) no-repeat;
    background-size: 83%;
    background-position: -98% 20%;
`;

const Component = ({ is404, ...props}) => (
    <MinimalLayout hasStormTrooper={true}>
        <Wrapper>
            <Outline style={{ marginTop: '25%' }}>
            <Title style={{ marginBottom: '10px' }}>{is404 ? TITLE_4O4 : TITLE}</Title>
            <Description style={{ marginBottom: '20px' }}>{DESCRIPTION}</Description>
            <Button onClick={ () => {browserHistory.push('/');} }style={{ background: BUTTON_COLOR }}>{BUTTON_TEXT}</Button>
            </Outline>
        </Wrapper>
    </MinimalLayout>
);

export default Component;