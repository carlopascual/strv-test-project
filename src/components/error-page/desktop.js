import React from 'react';
import styled from 'styled-components';

import LandingLayout from 'components/layouts/landing';
import Button from 'components/ui-elements/button';

import { browserHistory } from 'react-router';

const TITLE = 'Something went wrong.';
const TITLE_4O4 = '404 Error - Page not found.';
const TITLE_COLOR = '#323C46';

const DESCRIPTION = 'Seems like Darth Vader just hits our website and drops it down. Please press the refresh button and everything should be fine again.';
const DESCRIPTION_COLOR = '#949EA8';

const BUTTON_TEXT = 'REFRESH';
const BUTTON_COLOR = '#323C46';

const Outline = styled.div`
    width: 70%;
    margin-left: 25%;
`;

const Title = styled.div`
    font-size: 28px;
    color: ${TITLE_COLOR};
`;

const Description = styled.div`
    font-size: 18px;
    color: ${DESCRIPTION_COLOR};
`;

const Component = ({ is404, ...props}) => (
    <LandingLayout hasStormTrooper={true}>
        <Outline>
            <Title style={{ marginBottom: '10px' }}>{is404 ? TITLE_4O4 : TITLE}</Title>
            <Description style={{ marginBottom: '20px' }}>{DESCRIPTION}</Description>
            <Button onClick={ () => {browserHistory.push('/');} }style={{ background: BUTTON_COLOR }}>{BUTTON_TEXT}</Button>
        </Outline>
        
    </LandingLayout>
);

export default Component;