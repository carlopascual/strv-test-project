import { HAS_ERROR, HAS_NO_ERROR, SUBMIT_RESPONSE, LOGOUT } from './constants';

export const setLoginFormHasError = () => ({ type: HAS_ERROR });
export const setLoginFormHasNoError = () => ({ type: HAS_NO_ERROR });
export const submitResponse = (response) => ({ type: SUBMIT_RESPONSE, response });
export const logout = () => ({ type: LOGOUT });
