export const HAS_NO_ERROR = 'HAS_NO_ERROR';
export const HAS_ERROR = 'HAS_ERROR';
export const SUBMIT_RESPONSE = 'SUBMIT_RESPONSE';
export const LOGOUT = 'LOGOUT';