import { HAS_ERROR, HAS_NO_ERROR, SUBMIT_RESPONSE, LOGOUT } from './constants';

const initialState = { hasError: false, response: null };

const Reducer = (state = initialState , action) => {
    switch(action.type) {
        case HAS_NO_ERROR :
            return {
                ...state,
                hasError: false,
            }
        case HAS_ERROR : 
            return {
                ...state,
                hasError: true,
            }
        case SUBMIT_RESPONSE :
            return {
                ...state,
                hasError: false,
                response: action.response,
            }
        case LOGOUT :
            return {
                ...state,
                response: null,
            }
        case 'persist/REHYDRATE':
            return {
                ...state,
                response: (action.payload.login == null || action.payload.login === false) ? null : action.payload.login.response,
            }
        default:
            return state
    }
}

export default Reducer;