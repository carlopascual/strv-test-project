import React from 'react';
import styled from 'styled-components';
import Formous from 'formous';
import schema from './schema';

import RevealPasswordIcon from './reveal-password-icon';

import {
    ACTIVE_COLOR, 
    INACTIVE_COLOR,  
    ERROR_COLOR, 
    Title,
} from 'components/ui-elements/input/elements';

import Button from 'components/ui-elements/button/component';
import Input from 'components/ui-elements/input/component';

const EMAIL_TITLE = 'Email';
const PASSWORD_TITLE = 'Password';
const FORGOT_PASSWORD_TITLE = 'Forgot password?';

const SIGN_IN_TEXT = 'SIGN IN';

const ButtonWrapper = styled.div`
    display: flex;
    justify-content: ${props => props.center && 'center'};
    align-items:  ${props => props.center && 'center'};
    flex-direction: column;
`;

const PasswordWrapper = styled.div`
    display: inline-flex;
    width: 100%;
    align-items: center;

    border-bottom: 1px solid ${INACTIVE_COLOR};
    border-bottom: ${props => props.isFocused && `1px solid ${ACTIVE_COLOR}`}; 
    border-bottom: 
    &:focus {
        border-bottom: 1px solid ${ACTIVE_COLOR};
    }

    &.has-error {
        border-bottom: 1px solid ${ERROR_COLOR};
    }
`;

class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = { passwordShow: false, passwordFocus: false };

        this.onPasswordFocus = this.onPasswordFocus.bind(this);
        this.onPasswordBlur = this.onPasswordBlur.bind(this);
        this.showPasswordToggle = this.showPasswordToggle.bind(this);
    }

    onPasswordFocus(e) {
        this.setState({ passwordFocus: true });
    }

    onPasswordBlur(e) {
        this.setState({ passwordFocus: false });
    }

    showPasswordToggle(e) {
        this.setState({
            passwordShow: !this.state.passwordShow,
        });
    }

    render() {
        const {
            fields: { email, password },
            formSubmit,
            handleSubmit,
            hasError,
            isMobile,
        } = this.props;

        return (
            <form onSubmit={formSubmit(handleSubmit)}>
                <Title>{EMAIL_TITLE}</Title>
                <Input placeholder="Email" className={hasError && 'has-error'} style={{ marginBottom: '72px' }} value={email.value} {...email.events} />
                <Title>{PASSWORD_TITLE}</Title>
                <PasswordWrapper className={hasError && 'has-error'} isFocused={this.state.passwordFocus} style={{ marginBottom: '20px' }}>
                    <Input 
                        placeholder="Password" 
                        type={`${this.state.passwordShow ? 'input' : 'password'}`} 
                        {...password.events}                        
                        onFocus={(e) => { password.events.onFocus(e); this.onPasswordFocus(e); }}  // we need to overload password.event's onFocus with our own onFocus()
                        onBlur={(e) => { password.events.onBlur(e); this.onPasswordBlur(e); }} // we need to overload password.event's onBlur with our own onBlur()
                        style={{ border: 'none' }}
                        value={password.value}
                    />
                    <RevealPasswordIcon isActive={this.state.passwordShow} onClick={this.showPasswordToggle} />
                </PasswordWrapper>
                <ButtonWrapper center={isMobile}>
                    <Title className="hoverable" style={{ marginBottom: '40px', userSelect: 'none' }}><span>{FORGOT_PASSWORD_TITLE}</span></Title>                
                    <Button className="green" >{SIGN_IN_TEXT}</Button>                    
                </ButtonWrapper>
            </form>
        );
    } 
}

export default Formous(schema)(Component);
