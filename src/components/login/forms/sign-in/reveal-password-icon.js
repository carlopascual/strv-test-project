import React from 'react';
import styled from 'styled-components';

const DEFAULT_COLOR = '#E1E4E6';
const ACTIVE_COLOR = '#323C46';
const SELECTED_COLOR = '#a9a9a9';

const Svg = styled.svg`
    > g path {
        fill: ${props => props.isActive ? `${ACTIVE_COLOR}` : `${DEFAULT_COLOR}`};
    }

    &:active {
        > g path {
            fill: ${SELECTED_COLOR};
        }
    }
`;

const Component = ({ isActive, ...props }) => (
    <Svg isActive={isActive} width="22px" height="16px" viewBox="0 0 22 16" version="1.1" xmlns="http://www.w3.org/2000/svg" {...props}>
        <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="00-1-1-Log-In" transform="translate(-1177.000000, -539.000000)">
                <g id="Pass" transform="translate(720.000000, 535.000000)">
                    <g id="icon-show" transform="translate(456.000000, 0.000000)">
                        <polygon id="Stroke-1" strokeOpacity="0.00784313771" stroke="#000000" strokeWidth="1.33333335e-11" points="0 0 23.9999985 0 23.9999985 23.9999985 0 23.9999985"></polygon>
                        <path d="M11.9999993,4.49999973 C6.99999958,4.49999973 2.72999985,7.60999967 0.99999994,11.9999993 C2.72999985,16.3899983 6.99999958,19.4999988 11.9999993,19.4999988 C16.999999,19.4999988 21.2699991,16.3899983 22.9999986,11.9999993 C21.2699991,7.60999967 16.999999,4.49999973 11.9999993,4.49999973 Z M11.9999993,16.999999 C9.23999922,16.999999 6.99999958,14.7599993 6.99999958,11.9999993 C6.99999958,9.23999922 9.23999922,6.99999958 11.9999993,6.99999958 C14.7599993,6.99999958 16.999999,9.23999922 16.999999,11.9999993 C16.999999,14.7599993 14.7599993,16.999999 11.9999993,16.999999 Z M11.9999993,8.99999946 C10.3399995,8.99999946 8.99999946,10.3399995 8.99999946,11.9999993 C8.99999946,13.659999 10.3399995,14.9999991 11.9999993,14.9999991 C13.659999,14.9999991 14.9999991,13.659999 14.9999991,11.9999993 C14.9999991,10.3399995 13.659999,8.99999946 11.9999993,8.99999946 Z" id="Fill-2"></path>
                    </g>
                </g>
            </g>
        </g>
    </Svg>
);

export default Component;