import { connect } from 'react-redux';
import Component from './component';
import { setLoginFormHasError, submitResponse } from './redux/actions';
import { API_KEY } from 'services/constants';
import { browserHistory } from 'react-router';
import parse from 'parse-headers';

const handleRequest = (dispatch, email, password) => {
    var request = new XMLHttpRequest();

    request.open('POST', 'https://testproject-api-v2.strv.com/auth/native');

    request.setRequestHeader('Content-Type', 'application/json');
    request.setRequestHeader('APIKey', API_KEY);

    request.onreadystatechange = function () {
    if (this.readyState === 4) {
        // console.log('Status:', this.status);
        // console.log('Headers:', this.getAllResponseHeaders());
        // console.log('Body:', this.responseText);

        // success! submit Auth key
        if(this.status === 200) {
            const response = JSON.parse(this.response);
            response.authorization = parse(this.getAllResponseHeaders()).authorization;
            dispatch(submitResponse(response));  
            browserHistory.push('/dashboard');   
        }
        else if(this.status === 400) {
            dispatch(setLoginFormHasError());
        }
        else {
            browserHistory.push('/error');
            return;
        }
    }
    };

    var body = {
    'email': email,
    'password': password,
    };

    request.send(JSON.stringify(body));
};

const mapStateToProps = (state) => ({
    hasError: state.login.hasError,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    handleSubmit: (formStatus, fields) => { 
        if(!formStatus.touched) {
            dispatch(setLoginFormHasError());            
            alert('Please fill out the form.');
            return;
        }

        if(!formStatus.valid) {
            dispatch(setLoginFormHasError());                        
            alert('Please addres the errors in the form.');
            return;
        }

        handleRequest(dispatch, fields.email.value, fields.password.value);

    },
});

const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;