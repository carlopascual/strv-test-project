const required = name => ({
  critical: true,
  failProps: {
    message: `${name} is required`,
  },
  test(value) {
    return value !== '';
  },
});

export default {
  fields: {
    email: {
      tests: [
        required('Email'),
      ],
    },
    password: {
      tests: [
        required('Password'),
      ],
    },
  },
};