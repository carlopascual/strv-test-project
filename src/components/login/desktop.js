import React from 'react';
import styled from 'styled-components';

import Title from './title/';
import Login from './forms/sign-in/';

import LandingLayout from 'components/layouts/landing';

const TITLE = 'Sign in to Eventio.';
const TITLE_DETAILS = 'Enter your details below.';
const TITLE_DETAILS_ERROR = 'Oops! That email and password combination is not valid.';

const LoginWrapper = styled.div`
    width: 60%;
    max-width: 480px;
`;
const Component = (props) => {
    // prevent scrollbar from appearing when on this page
    document.body.style.overflow = 'hidden';
    document.body.style.backgroundColor = '#FFFFFF';

    return(
        <LandingLayout>
            <LoginWrapper style={{ margin: '0 auto'}}>
                <Title style={{ marginBottom: '40px' }} titleDetails={TITLE_DETAILS} titleDetailsError={TITLE_DETAILS_ERROR}>{TITLE}</Title>
                <Login />                    
            </LoginWrapper>
        </LandingLayout>
    );
};

Component.displayName = 'LoginDesktopComponent';
export default Component;
