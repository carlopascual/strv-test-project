import React from 'react';
import styled from 'styled-components';
import Login from './forms/sign-in/';

import MinimalLayout from 'components/layouts/minimal';

const TITLE = 'Sign in to Eventio.';
const DESCRIPTION = 'Enter your details below.';

const TITLE_COLOR = '#323C46';
const DESCRIPTION_COLOR = '#949EA8';

const Title = styled.div`
    color: ${TITLE_COLOR};
    font-size: 22px;
    text-align: center;
`;

const Description = styled.div`
    color: ${DESCRIPTION_COLOR};
    font-size: 14px;
    text-align: center;    
`;

const LoginWrapper = styled.div`
    padding: 10%;
`;

const Component = (props) => (
 <MinimalLayout>
     <Title style={{ marginBottom: '20px' }}>{TITLE}</Title>
     <Description>{DESCRIPTION}</Description>
     <LoginWrapper>
         <Login isMobile style={{ width: '90%', margin: '0 auto'}}/>     
     </LoginWrapper>
 </MinimalLayout>
);

export default Component;
