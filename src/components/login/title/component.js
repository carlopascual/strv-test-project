import React from 'react';
import styled from 'styled-components';

const HEADER_COLOR = '#323C46';
const DETAILS_COLOR = '#949EA8';
const DETAILS_ERROR_COLOR = '#FF4081';

const Header = styled.div`
    font-size: 28px;
    color: ${HEADER_COLOR};
`;

const Details = styled.div`
    font-size: 18px;
    color: ${DETAILS_COLOR};

    &.has-error {
        color: ${DETAILS_ERROR_COLOR};
    }
`;

const Component = ({ children, titleDetails, titleDetailsError, hasError, style, ...props }) => (
    <div style={style}>
        <Header>{children}</Header>
        <Details className={hasError && 'has-error'}> {hasError ? titleDetailsError : titleDetails } </Details>
    </div>
);

Component.displayName = 'TitleComponent';
export default Component;
