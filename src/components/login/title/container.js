import { connect } from 'react-redux';
import Component from './component';

const mapStateToProps = (state) => ({
    hasError: state.login.hasError,
});

const Composer = connect(
    mapStateToProps,
    null,
)(Component);

export default Composer;