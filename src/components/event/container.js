import _ from 'lodash';
import Component from './component';
import { connect } from 'react-redux';
import retrieveEvent from 'services/events/retrieve-current-event';
import deleteEvent from 'services/events/delete-event';
import { setAsView, setAsEdit } from 'services/events/redux/actions';
import { store } from 'App';

const mapStateToProps = (state) => ({
    event: state.events.currentEvent,
    view: state.events.view,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    retrieveEvent: (eventID) => { retrieveEvent(dispatch, eventID); },
    setView: () => { dispatch(setAsView()); },
    setEdit: () => { dispatch(setAsEdit()); },
    deleteEvent: () => { deleteEvent(dispatch, store.getState().login.response.authorization, store.getState().events.currentEvent._id); },
});


const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);


export default Composer;