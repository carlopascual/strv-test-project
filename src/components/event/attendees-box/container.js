import Component from './component';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
    ownerID: state.login.response.id,
})

const Composer = connect(
    mapStateToProps,
    null,
)(Component)

export default Composer;