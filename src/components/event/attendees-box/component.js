import React from 'react';
import styled from 'styled-components';

import AttendeButton from './attendee';

const TITLE_TEXT = 'Attendees';
const TITLE_COLOR = '#323C46';

const Outline = styled.div`
    background: #FFFFFF;
    width: 100%;
    height: 100%;
    box-shadow: 0px 2px 3px #888888;
`;

const Title = styled.div`
    color: ${TITLE_COLOR};
    font-size: 22px;
`;

const ButtonWrapper = styled.div`
`;

const Component = ({ ownerID, children, ...props }) => (
    <Outline {...props}>
        <div style={{ padding: '30px' }}>
            <Title style={{ marginBottom: '10px' }}>{TITLE_TEXT}</Title>
            <ButtonWrapper>
                {
                    children !== null && children.map(attendee => (
                        <div style={{ display: 'inline-block' }}>
                            <AttendeButton isYou={attendee.id === ownerID } style={{ marginRight: '10px', marginBottom: '10px' }} key={attendee.id}>{attendee.firstName} {attendee.lastName}</AttendeButton>
                        </div>
                    ))
                }
            </ButtonWrapper>
        </div>
    </Outline>
);

export default Component;