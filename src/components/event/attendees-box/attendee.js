import React from 'react';
import styled from 'styled-components';

const BUTTON_BACKGROUND_COLOR = '#D9DCE1';
const BUTTON_FONT_COLOR = '#949EA8';

const YOU_BACKGROUND_COLOR = '#FFFFFF';
const YOU_TEXT = 'You';

const Outline = styled.div`
    width: inherit;
    border-radius: 50px;
    background-color: ${ props => props.isYou ? YOU_BACKGROUND_COLOR : BUTTON_BACKGROUND_COLOR };
    border: 2px solid ${BUTTON_BACKGROUND_COLOR};    
    text-align: center;
    color: ${BUTTON_FONT_COLOR};
    font-size: 13px;
    display: inline-block;
    padding: 3px 13px 3px 13px;
`;

const Component = ({isYou, children, ...props }) => (
    <Outline isYou={isYou} {...props}>{isYou ? YOU_TEXT : children}</Outline>
);

export default Component;