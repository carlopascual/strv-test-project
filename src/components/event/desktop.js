import React from 'react';
import styled from 'styled-components';
import MediaQuery from 'react-responsive';

import StandardLayout from 'components/layouts/standard';
import MainWindow from './main-window';
import AttendeesBox from './attendees-box';
import DeleteIcon from './delete-icon';

import AddWidget from 'components/ui-elements/add-widget';

import { VIEW, EDIT } from 'services/events/redux/constants';

const BACK_BUTTON_COLOR = '#323C46';
const SUBHEADER_COLOR = '#A9AEB4';
const SUBHEADER_BACKGROUND_COLOR = 'rgb(249, 249, 251)';

const DELETE_EVENT_COLOR = '#FF4081';
const DELETE_EVENT_HOVER_COLOR = '#E73370';

const DELETE_EVENT_TEXT = 'DELETE EVENT';

const ItemOutline = styled.div`
    padding-left: 100px;
    padding-right: 100px;
    display: flex;
`;

const BackButton = styled.div`
    cursor: pointer;

    a {
        color: ${BACK_BUTTON_COLOR};
        text-decoration: none;
    }
`;

const SubHeader = styled.div`
    height: 30px;
    color: ${SUBHEADER_COLOR};
    font-size: 12px;
    background-color: ${SUBHEADER_BACKGROUND_COLOR};
    display: flex;
    margin: 0 100px 0 100px;
    align-items: center;

    span {
        vertical-align: middle;
        display: inline-block;
    }
`;

const DeleteEvent = styled.div`
    color: ${DELETE_EVENT_COLOR};
    display: flex;

    g > path {
        fill: ${DELETE_EVENT_COLOR};
    }

    &:hover {
        color: ${DELETE_EVENT_HOVER_COLOR};
        cursor: pointer;
        
        g > path {
            fill: ${DELETE_EVENT_HOVER_COLOR};
        }
    }
`;

class Component extends React.Component {
    componentWillMount() {
        this.props.retrieveEvent(this.props.routeParams.id);
    }

    render() {
        return(
            <StandardLayout
                subHeader={
                    <div>
                        <BackButton style={{ position: 'absolute', top: '30px', left: '0', right:'0', textAlign: 'center' }}><a href="/dashboard">{'← Back to events'}</a></BackButton>
                        <SubHeader style={{ position: 'relative' }}>
                            <span>{`Event detail: ${!(this.props.event == null) && this.props.event.id}`}</span>
                            {this.props.view === EDIT && <DeleteEvent onClick={this.props.deleteEvent} style={{ position: 'absolute', right: 0 }}><DeleteIcon style={{ marginRight: '5px' }} />{DELETE_EVENT_TEXT}</DeleteEvent>}
                        </SubHeader>
                    </div>
                }>
                {this.props.event !== null && <ResponsiveContent event={this.props.event} view={this.props.view} setEdit={this.props.setEdit} setView={this.props.setView} />}
                {this.props.view === VIEW && <AddWidget style={{ position: 'fixed', bottom: '30px', right: '30px' }} />}
            </StandardLayout>
        );
    }
};

const ResponsiveContent = ({ view, event, setView, setEdit, ...props }) => (
    <div {...props}>
        <MediaQuery minWidth={661}>
            <ItemOutline style={{ margin: '0 auto' }}>
                 <MainWindow style={{ width: '75%', marginRight: '20px', height: 'inherit' }} setEdit={setEdit} view={view} formButton={
                    <AddWidget onClick={()=>{}} isEdit style={{ position: 'fixed', bottom: '30px', right: '30px' }} />                     
                 }>{event}</MainWindow>
                <AttendeesBox style={{ width: '50%', height: 'inherit' }}>{event.attendees}</AttendeesBox>
            </ItemOutline>
        </MediaQuery>
        <MediaQuery maxWidth={660}>
            <div>
            <ItemOutline style={{ margin: '0 auto 20px' }}>
                 <MainWindow style={{ width: '100%' }} setEdit={setEdit} view={view} formButton={
                    <AddWidget onClick={()=>{}} isEdit style={{ position: 'fixed', bottom: '30px', right: '30px' }} />                    
                 }>{event}</MainWindow>                  
            </ItemOutline>
            <ItemOutline style={{ margin: '0 auto' }}>
                <AttendeesBox>{event.attendees}</AttendeesBox>
            </ItemOutline>
            </div>
        </MediaQuery>
    </div>
);

export default Component;
