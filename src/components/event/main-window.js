import React from 'react';

import GridItem from 'components/dashboard/views/grid/grid-item';
import EventDetailsForm from 'components/ui-elements/event-details-form';

import { VIEW } from 'services/events/redux/constants';
import { FormOutline, InnerFormPadding } from 'components/ui-elements/event-details-form/elements';

const Component = ({view, setEdit, formButton, children, ...props}) => (
    <div {...props} >
        {
            view === VIEW ?
            <GridItem onEdit={() => {setEdit();}} isOnEventPage>{children}</GridItem>
            :
            <FormOutline>
                <InnerFormPadding>
                    <EventDetailsForm formButton={formButton} style={{ width: '100%' }}/>
                </InnerFormPadding>
            </FormOutline>
        }
    </div>
);

export default Component;
