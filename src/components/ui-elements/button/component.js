import React from 'react';
import styled from 'styled-components';

const GREEN_BUTTON_COLOR = '#22D486';
const GREEN_BUTTON_HOVER_COLOR = '#20BE78';

const PINK_BUTTON_COLOR = '#FF4081';
const PINK_BUTTON_HOVER_COLOR = '#E73370';

const EDIT_COLOR = '#D9DCE1';
const EDIT_HOVER_COLOR = '#C4C9D1';
const REFRESH_COLOR = '#323C46';

const BUTTON_TEXT_COLOR = '#FFFFFF';

const Button = styled.button`
    width: 240px;
    height: 57px;
    border: 2px solid transparent;
    border-radius: 5px;
    font-face: 'Hindi-SemiBold';
    color: ${BUTTON_TEXT_COLOR};
    font-size: 16px;

    &:focus {
        outline: none;
    }

    &:hover {
        cursor: pointer;
    }

    &.green {
        background: ${GREEN_BUTTON_COLOR};
        &:hover {
            background: ${GREEN_BUTTON_HOVER_COLOR};
        }
    }

    &.pink {
        background: ${PINK_BUTTON_COLOR};
        &:hover {
            background: ${PINK_BUTTON_HOVER_COLOR};
        }
    }

    &.edit {
        background: ${EDIT_COLOR};
        &:hover {
            background: ${EDIT_HOVER_COLOR};            
        }
    }

    &.refresh {
        background: ${REFRESH_COLOR};
    }

    &.small {
        width: 100px;
        height: 32px;
        font-size: 14px;
    }
`;

const Component = ({children, ...props}) => (
    <Button {...props}  > {children} </Button>
);

export default Component;
