import React from 'react';
import styled from 'styled-components';

import { browserHistory } from 'react-router';

import Circle from 'components/ui-elements/circle';

const DEFAULT_COLOR = '#323C46';
const EDIT_COLOR = '#22D486';

const Outline = styled.div`
    cursor: pointer;
`;

const Button = styled.button`
    background-color: transparent;
    border: none;
    cursor: pointer;
    outline: none;
`;
const Component = ({ isEdit, onClick, ...props }) => (
    <Outline {...props}>
        <Button type={isEdit && 'submit'} onClick={() => { onClick || browserHistory.push('/event/create'); }}>
            <Circle 
                backgroundColor={isEdit ? `${EDIT_COLOR}` : `${DEFAULT_COLOR}`}
                fontColor="#FFFFFF"
                style={{ boxShadow: '0px 6px 9px rgba(0,0,0,0.15)', lineHeight: '39px'}}
            >
            {isEdit ? '✓' : '+'}
            </Circle>
        </Button>
            
    </Outline>
   
);

export default Component;