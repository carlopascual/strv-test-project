import React from 'react';
import styled from 'styled-components';

const Circle = styled.div`
    border-radius: 50%;
    width: 40px;
    height: 40px;
    text-align: center;
    font-size: 14px;
    line-height: 43px;
    background-color: ${props => props.backgroundColor};
    color: ${props => props.fontColor};
`;

const Component = ({ backgroundColor, fontColor, children, ...props }) => (
    <Circle backgroundColor={backgroundColor} fontColor={fontColor} {...props}>{children}</Circle>
);

export default Component;