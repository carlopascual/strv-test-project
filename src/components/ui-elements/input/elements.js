import styled from 'styled-components';

export const ACTIVE_COLOR = '#323C46';
export const INACTIVE_COLOR = '#DAE1E7';
export const PLACEHOLDER_COLOR = '#D8D8D8';
export const ERROR_COLOR = '#FF4081';

export const Title = styled.div`
    font-size: 14px;
    color: ${PLACEHOLDER_COLOR};

    &.hoverable {
        color: ${PLACEHOLDER_COLOR};
    
        span:hover {
            cursor: pointer;
            color: ${ACTIVE_COLOR};        
        }
    }
`;

export const ErrorMessage = styled.div`
    color: ${ERROR_COLOR};
`;
