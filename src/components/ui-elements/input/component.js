import React from 'react';
import styled from 'styled-components';
import { ACTIVE_COLOR, INACTIVE_COLOR, ERROR_COLOR, PLACEHOLDER_COLOR } from './elements';

const Input = styled.input`
    width: 100%;
    height: 33px;
    border: none;
    border-bottom: 1px solid ${INACTIVE_COLOR};
    font-size: 18px;
    background: transparent;
    
    &:focus {
        outline: none;
        border-bottom: 1px solid ${ACTIVE_COLOR};        
    }

    &.has-error {
        border-bottom: 1px solid ${ERROR_COLOR};   
    }

    &[type="password"] {
        color: ${ACTIVE_COLOR};
    }

    &::-webkit-input-placeholder {
        color: ${PLACEHOLDER_COLOR};
        font-size: 18px;
    }
`;

const Component = ({hasError, ...props}) => (
    <Input className={hasError && 'has-error'} {...props} />
);

export default Component;
