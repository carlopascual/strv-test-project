import React from 'react';
import LOGO_SVG from 'static/img/logo/logo.svg';

const Component = ({black, ...props}) => (
    <div style={{ textAlign: 'center' }} {...props}>
        <a href="/">
            <img style={{ paddingTop: '8px', filter: `${black && 'invert(1)'}` }} src={LOGO_SVG} alt="logo" />
        </a>
    </div>
    
);

export default Component;
