import React from 'react';
import Formous from 'formous';
import schema from './schema';
import moment from 'moment';

import Input from 'components/ui-elements/input';
import { ErrorMessage } from 'components/ui-elements/input/elements';
import  { MOMENT_DATE_FORMAT, MOMENT_TIME_FORMAT } from './schema';

class Component extends React.Component {
    constructor(props) {
        super(props);

         if(window.location.pathname !== '/event/create') {
            this.props.setDefaultValues({
                title: this.props.currentEvent.title,
                description: this.props.currentEvent.description,
                date:moment(this.props.currentEvent.startsAt).format(MOMENT_DATE_FORMAT),
                time:moment(this.props.currentEvent.startsAt).format(MOMENT_TIME_FORMAT),
                capacity: this.props.currentEvent.capacity,
            });
        }
    }

    render() {
        const {
            fields: {title, description, date, time, capacity},
            formSubmit,
            handleEventCreation,
            handleEventUpdate,
            formButton,
            currentEvent,
            ...props
        } = this.props;

        return (
            <div {...props}>
                <form onSubmit={formSubmit(window.location.pathname === '/event/create' ? handleEventCreation : handleEventUpdate)}>
                    <Input hasError={title.failProps} value={title.value} style={{ marginBottom: `${title.failProps ? '10px' : '39px'}` }} placeholder="Title" {...title.events} />
                    <ErrorMessage>{title.failProps && title.failProps.message}</ErrorMessage>

                    <Input hasError={description.failProps} value={description.value} style={{ marginBottom: `${description.failProps ? '10px' : '39px'}` }} placeholder="Description" {...description.events} />
                    <ErrorMessage>{description.failProps && description.failProps.message}</ErrorMessage>

                    <Input hasError={date.failProps} value={date.value} style={{ marginBottom: `${date.failProps ? '10px' : '39px'}` }} placeholder="Date (MM-DD-YYYY)" {...date.events} />
                    <ErrorMessage>{date.failProps && date.failProps.message}</ErrorMessage>
                    
                    <Input hasError={time.failProps} value={time.value} style={{ marginBottom: `${time.failProps ? '10px' : '39px'}` }} placeholder="Time (24hr HH:MM)" {...time.events} />
                    <ErrorMessage>{time.failProps && time.failProps.message}</ErrorMessage>
                    
                    <Input hasError={capacity.failProps} value={capacity.value} style={{ marginBottom: `${capacity.failProps ? '10px' : '39px'}` }} placeholder="Capacity" {...capacity.events} />
                    <ErrorMessage>{capacity.failProps && capacity.failProps.message}</ErrorMessage>
                    
                    {formButton}
                </form>
            </div>
        );
    }
}
export default Formous(schema)(Component);
