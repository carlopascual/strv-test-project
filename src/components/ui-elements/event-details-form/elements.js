import styled from 'styled-components';

export const FormOutline = styled.div`
    background: #FFFFFF;
    width: 100%;
    height: 100%;
    box-shadow: 0px 2px 3px #888888;
`;

export const InnerFormPadding = styled.div`
    padding: 60px 30px;
`;