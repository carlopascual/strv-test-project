import moment from 'moment';
import { connect } from 'react-redux';
import Component from './component';
import  { MOMENT_DATE_FORMAT, MOMENT_TIME_FORMAT } from './schema';
import { store } from 'App';

import { setAsView } from 'services/events/redux/actions';

import createEvent from 'services/events/create-event';
import updateEvent from 'services/events/update-event';

const parseDateTimeMoment = (date, time) => {
    const dateTime = moment(date + ' ' + time, `${MOMENT_DATE_FORMAT} ${MOMENT_TIME_FORMAT}`);
    return dateTime;
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    handleEventCreation: (formStatus, fields) => {

        if(!formStatus.touched) {
            alert('Please fill out the form.');
            return;
        }

        if(!formStatus.valid) {
            alert('Please addres the errors in the form.');
            return;
        }
        
        createEvent(
            dispatch,
            store.getState().login.response.authorization, 
            fields.title.value, 
            fields.description.value,
            parseDateTimeMoment(fields.date.value, fields.time.value).toISOString(),
            fields.capacity.value,
        );
    },
    handleEventUpdate: (formStatus, fields) => {

        if(!formStatus.touched) {
            dispatch(setAsView());            
            return;
        }

        if(!formStatus.valid) {
            alert('Please addres the errors in the form.');
            return;
        }
        
        const currentEvent = store.getState().events.currentEvent;
        const dateTimeISO = moment(fields.date.value + ' ' + fields.time.value, `${MOMENT_DATE_FORMAT} ${MOMENT_TIME_FORMAT}`).toISOString();

        const body = {};        
        if(currentEvent.title !== fields.title.value) body.title = fields.title.value;
        if(currentEvent.description !== fields.description.value) body.description = fields.description.value;
        if(currentEvent.startsAt !== dateTimeISO) body.startsAt = dateTimeISO;
        if(currentEvent.capacity !== fields.capacity.value) body.capacity = fields.capacity.value;

        updateEvent (
            dispatch,
            store.getState().login.response.authorization,
            currentEvent._id,
            body,
        );
    },
});

const mapStateToProps = (state) => ({
    currentEvent: state.events.currentEvent,
});


const Composer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);

export default Composer;
