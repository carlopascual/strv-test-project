import moment from 'moment';

export const MOMENT_DATE_FORMAT = 'MM-DD-YYYY';
export const MOMENT_TIME_FORMAT = 'HH:mm';

const hasValidTime = (value) => moment(value, MOMENT_TIME_FORMAT, true).isValid();
const hasValidDate = (value) => moment(value, MOMENT_DATE_FORMAT, true).isSame(moment(), 'day') || moment(value, MOMENT_DATE_FORMAT, true).isAfter(moment(), 'day'); 
const checkBothDateAndTime = (date, time) => moment(date + ' ' + time, `${MOMENT_DATE_FORMAT} ${MOMENT_TIME_FORMAT}`).isAfter(moment()); 

const required = name => ({
  critical: true,
  failProps: {
    message: `${name} is required`,
  },
  test(value) {
    return value !== '';
  },
});

const isNumber = name => ({
  critical: true,
  failProps: {
    message: `${name} must be an integer`,
  },
  test(value) {
    return value % 1 === 0;
  },
});

const isGreaterThan1 = name => ({
  critical: true,
  failProps: {
    message: `${name} must be greater than 1`,
  },
  test(value) {
    return value > 1;
  },
});


const isDate = name => ({
  critical: true,
  failProps: {
    message: `Invalid date`,
  },
  test(value) {
    return moment(value, MOMENT_DATE_FORMAT, true).isValid();
  },
});

const isValidDate = name => ({
  critical: true,
  failProps: {
    message: `${name} must be in the future`,
  },
  test(value, fields) {

    // check if has valid time
    if(hasValidTime(fields.time.value)) {
      return checkBothDateAndTime(value, fields.time.value);
    }

    return hasValidDate(value);
  },
});

const isTime = name => ({
  critical: true,
  failProps: {
    message: `Invalid time`,
  },
  test(value) {
    return moment(value, MOMENT_TIME_FORMAT, true).isValid();
  },
});

const isValidTime = name => ({
  critical: true,
  failProps: {
    message: `${name} must be in the future`,
  },
  test(value, fields) {

    // check if has valid date
    if(hasValidDate(fields.date.value)) {
      // check both
      return checkBothDateAndTime(fields.date.value, value);
    }
    
    return hasValidTime(value);
  },
});

export default {
  fields: {
    title: {
      tests: [
        required('Title'),
      ],
    },
    description: {
      tests: [
        required('Description'),
      ],
    },
    date: {
      tests: [
        required('Date'),
        isDate(),
        isValidDate('Date'),
      ],
    },
    time: {
      tests: [
        required('Time'),
        isTime(),
        isValidTime('Time'),
      ],
    },
    capacity: {
      tests: [
        required('Capacity'),
        isNumber('Capacity'),
        isGreaterThan1('Capacity'),
      ],
    },
  },
};