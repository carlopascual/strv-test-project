import React from 'react';
import styled from 'styled-components';

export const ITEM_TEXT_COLOR = '#9CA5AF';

const Outline = styled.div`
    background: white;
    z-index: 1;
    padding: 12px;
    box-shadow: 0px 5px 15px rgba(0,0,0,0.2);
    width: 110%;
    border-radius: 18px;
    padding-left: 15px;
`;

const Triangle = styled.div`
  position: absolute;
  width: 0;
  height: 0;
  top: 0;
  box-sizing: border-box;
  border: 8px solid black;
  border-color: transparent transparent #ffffff #ffffff;
  transform-origin: 0 0;
  transform: rotate(134deg);
  box-shadow: -2px 2px 4px -1px rgba(0, 0, 0, 0.1);
`;

export const DropdownItem = styled.div`
    color: ${ITEM_TEXT_COLOR};
    font-size: 14px;
    line-height: 26px;
`;

const Component = ({children, triangleLeft, ...props}) => (
    <Outline {...props}>
        <Triangle style={{ left: triangleLeft }}/>
        {children}
    </Outline>
);

export default Component;