import styled from 'styled-components';

const ITEM_TEXT_COLOR = '#9CA5AF';
const ITEM_HOVER_COLOR = '#323C46';

export const DropdownItem = styled.div`
    color: ${ITEM_TEXT_COLOR};
    font-size: 14px;
    line-height: 26px;
    cursor: pointer;
    text-align: left;
    &:hover {
        color: ${ITEM_HOVER_COLOR};
    }
`;
