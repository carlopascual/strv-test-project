import React from 'react';
import styled from 'styled-components';

import MinimalLayout from 'components/layouts/minimal';

import NewEventForm from './form/';

const FormWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const Component = () => (
    <MinimalLayout hasX>
        <FormWrapper style={{ marginTop: '10px' }}>
            <NewEventForm />            
        </FormWrapper>
    </MinimalLayout>
);

export default Component;