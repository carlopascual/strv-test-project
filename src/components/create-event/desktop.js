import React from 'react';
import styled from 'styled-components';

import MinimalLayout from 'components/layouts/minimal';

import NewEventForm from './form/';

const FormWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const Component = () => (
    <MinimalLayout hasCloseButton>
        <FormWrapper style={{ marginTop: '50px' }}>
            <NewEventForm style={{ maxWidth: '500px' }} />            
        </FormWrapper>
    </MinimalLayout>
);

export default Component;