import React from 'react';
import styled from 'styled-components';

import EventDetailsForm from 'components/ui-elements/event-details-form/';
import Button from 'components/ui-elements/button';
import { FormOutline, InnerFormPadding } from 'components/ui-elements/event-details-form/elements';

const TITLE = 'Create new event';
const DESCRIPTION = 'Enter details below.';

const BUTTON_TEXT = 'CREATE NEW EVENT';

const TITLE_COLOR = '#313C46';
const DESCRIPTION_COLOR = '#949EA8';

const Title = styled.div`
    color: ${TITLE_COLOR};
    font-size: 28px;
    text-align: center;
`;

const Description = styled.div`
    color: ${DESCRIPTION_COLOR};
    font-size: 18px;
    text-align: center;
`;

const Component = ({...props}) => (
    <FormOutline {...props}>
    <InnerFormPadding>
        <Title>{TITLE}</Title>
        <Description style={{ marginBottom: '30px' }}>{DESCRIPTION}</Description>
        <EventDetailsForm formButton={
            <div style={{ display: 'flex', marginTop: '20px' }} >
                <Button style={{ margin: '0 auto' }} className="green">{BUTTON_TEXT}</Button>            
            </div>
        }/>
    </InnerFormPadding>
    </FormOutline>
);

export default Component;

