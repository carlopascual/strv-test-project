import React from 'react';

import Responsive from 'services/responsive';
import Desktop from './desktop';
import Mobile from './mobile';

const Component = ({ ...props }) => (
    <Responsive desktop={<Desktop {...props} />} mobile={<Mobile {...props} />}/>
);

export default Component;
