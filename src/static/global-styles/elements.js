import styled, { injectGlobal } from 'styled-components';

import HIND_LIGHT from 'static/fonts/hind/Hind-Light.ttf';
import HIND_REGULAR from 'static/fonts/hind/Hind-Regular.ttf';
import HIND_MEDIUM from 'static/fonts/hind/Hind-Medium.ttf';
import HIND_SEMIBOLD from 'static/fonts/hind/Hind-SemiBold.ttf';
import HIND_BOLD from 'static/fonts/hind/Hind-Bold.ttf';

import PLAYFAIR_DISPLAY_REGULAR from 'static/fonts/playfair-display/PlayfairDisplay-Regular.ttf';
import PLAYFAIR_DISPLAY_ITALIC from 'static/fonts/playfair-display/PlayfairDisplay-Italic.ttf';
import PLAYFAIR_DISPLAY_BOLD from 'static/fonts/playfair-display/PlayfairDisplay-Bold.ttf';
import PLAYFAIR_DISPLAY_BOLD_ITALIC from 'static/fonts/playfair-display/PlayfairDisplay-BoldItalic.ttf';
import PLAYFAIR_DISPLAY_BLACK from 'static/fonts/playfair-display/PlayfairDisplay-Black.ttf';

// inject global styles
// eslint-disable-next-line
injectGlobal `
    @font-face {
        font-family: 'Hind-Light';
        src: url(${HIND_LIGHT});
    }

    @font-face {
        font-family: 'Hind-Regular';
        src: url(${HIND_REGULAR});
    }

    @font-face {
        font-family: 'Hind-Medium';
        src: url(${HIND_MEDIUM});
    }

    @font-face {
        font-family: 'Hind-SemiBold';
        src: url(${HIND_SEMIBOLD});
    }

    @font-face {
        font-family: 'Hind-Bold';
        src: url(${HIND_BOLD});
    }

    @font-face {
        font-family: 'Playfair-Display-Regular';
        src: url(${PLAYFAIR_DISPLAY_REGULAR});
    }

    @font-face {
        font-family: 'Playfair-Display-Italic';
        src: url(${PLAYFAIR_DISPLAY_ITALIC});
    }

    @font-face {
        font-family: 'Playfair-Display-Bold';
        src: url(${PLAYFAIR_DISPLAY_BOLD});
    }

    @font-face {
        font-family: 'Playfair-Display-BoldItalic';
        src: url(${PLAYFAIR_DISPLAY_BOLD_ITALIC});
    }

    @font-face {
        font-family: 'Playfair-Display-Black';
        src: url(${PLAYFAIR_DISPLAY_BLACK});
    }

    body {
        // make sure that body takes the whole width of the screen
        min-height: 100%; 
        margin: 0;
        padding: 0;
        
        font-family: 'Hind-Regular';
        font-size: 18px;
    }

    html {
        height: 100%;
    }

    ::-webkit-input-placeholder {
        font-family: 'Hind-Regular';
    }
`;

// empty style that we import to load the global styles
const ApplicationStyle = styled.div`
`;

export default ApplicationStyle;