### STRV TEST PROJECT

carlo pascual

Includes all basic features + responsiveness!

### Points for extension/improvement:

1. Forgot password + Add new user was not implemented due to time constraints. (And only `add new user` was on the API?)
2. Many new technologies (especially CSS Grid) may not work on older browsers. So cross-browser compatibility could be something to be worked on.